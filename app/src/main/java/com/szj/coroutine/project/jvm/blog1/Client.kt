package com.szj.coroutine.project.jvm.blog1

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

/*
 * 作者: 史大拿
 * 时间: 2023/2/9$ 13:27$
 */
//
//fun main() = runBlocking<Unit> {
//    printlnThread("main start")
//
//    // runBlocking 子协程 // TODO 协程1
//    launch {
//        delay(1000)
//        printlnThread("this launch")
//    }
//
//    // 不是 runBlocking子协程 // TODO 协程2
//    val globalJob = GlobalScope.launch {
//        delay(2000)
//        printlnThread("global launch")
//    }
//    globalJob.join()
//
//    // 不是 runBlocking子协程 // TODO 协程3
//    val scope = CoroutineScope(Dispatchers.IO)
//    val customJob = scope.launch {
//        delay(2000)
//        printlnThread("custom launch")
//    }
//    customJob.join()
//
//    printlnThread("main end")
//}



// TODO ==========================================================
//fun main() = runBlocking<Unit> {
//    println("main start")
//
//    val job = launch {
//        println("launch 执行了 $coroutineContext")
//    }
//
//
//    val deferred = async {
//        println("async 执行了 $coroutineContext")
//        delay(1000)
//        "我是async返回结果"
//    }
//    println("async result ${deferred.await()}")
//
//    println("main end")
//}

//
// TODO ==========================================================
//
//fun main() = runBlocking<Unit> {
//    println("main start")
//
//    val job = launch {
//        try {
//            (0..100).forEachIndexed { index, _ ->
//                delay(1000)
//                println("launch $index")
//            }
//        } catch (e: Exception) {
//            println("协程被取消了 $e")
//        }
//    }
//
//    // 协程执行完成监听
//    job.invokeOnCompletion {
//        println("协程执行完毕${it}")
//    }
//
//    delay(5500)
//
//    // 取消协程
//    job.cancel()
//
//    println("main end")
//}

//
// TODO ==========================================================
//
//fun main() = runBlocking<Unit> {
//    println("main start")
//
//    val job = launch {
//        println("launch执行")
//    }
//
//    job.cancel()
//    job.join()
//
//
//
//    printlnJob(job)
//
//    println("main end")
//}
//
//fun printlnJob(job: Job) {
//    println(
//        "isActive:${if (job.isActive) "活跃" else "非活跃状态"}" +
//                "\tisCompleted:${if (job.isCompleted) "协程执行完成" else "协程还没执行完"}" +
//                "\tisCancelled:${if (job.isCancelled) "协程被取消" else "协程没有被取消"}"
//    )
//}


// TODO ==========================================================

fun main() = runBlocking<Unit> {
//    println("main start")


    launch(Dispatchers.IO) {

        launch(Dispatchers.Unconfined) {
            printlnThread("main start")
        }
    }

//    var i = 0
//    val job = launch(Dispatchers.Default) {
//        // isActive 是否是活跃状态
//        try {
//            while (true) {
//                coroutineContext[Job]?.ensureActive()
//                // ensureActive() // 直接调用也可以，系统帮我们扩展了
//                printlnThread("i = ${i++}")
//            }
//        } catch (e: Exception) {
//            println("捕获到了异常:$e")
//        }
//    }
//
//    delay(20)
//    job.cancelAndJoin()
//
//    println("main end")


}
