package com.szj.coroutine.project.jvm.blog2

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.*

/*
 * 作者: 史大拿
 * 时间: 2023/2/10$ 09:32$
 */

fun main() = runBlocking<Unit> {
    printlnThread("main")
//    Dispatchers.setMain(Dispatchers.Unconfined)

    launch(context = Dispatchers.IO) {
        printlnThread("launch2")
        launch {
            printlnThread("launch3")
            launch {

                printlnThread("launch4")
                withContext(context = Dispatchers.Main) {
                    printlnThread("launch4")
                }
            }
        }
    }
}

// TODO ==================================
//fun main() = runBlocking<Unit> {
//    printlnThread("main") // @coroutine#1
//
//    // 因为JVM平台没有Main线程，Main只是针对Android平台，所以需要添加这行代码【官方给的解决办法】
//    // issues : https://github.com/Kotlin/kotlinx.coroutines/issues/3244
//    // implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")
//    Dispatchers.setMain(Dispatchers.Unconfined)
//
//    launch(Dispatchers.IO) {// @coroutine#2
//        launch { // @coroutine#3
//            launch {// @coroutine#4
//                withContext(Dispatchers.Main) {
//                    printlnThread("launch4")
//                }
//            }
//        }
//    }
//}

// TODO ==============
//fun main() = runBlocking<Unit> {
//    printlnThread("main")
//    launch(Dispatchers.IO) {
//
//        Dispatchers.setMain(Dispatchers.Unconfined)
//        printlnThread("launch1")
//        launch(Dispatchers.Unconfined) {
//            launch(Dispatchers.Unconfined) {
//                launch(Dispatchers.Unconfined) {
//                    launch(Dispatchers.Unconfined) {
//                        launch(Dispatchers.Unconfined) {
//                            printlnThread("launch2")
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    withContext(Dispatchers.Main) {
//        launch(Dispatchers.Unconfined) {
//            printlnThread("launch3")
//        }
//    }
//
//    launch(Dispatchers.Main) {
//            printlnThread("launch4")
//            printlnThread("launch5")
//            printlnThread("launch6")
//    }
//}


// TODO =============================================
//suspend fun main() {
//    val scope = CoroutineScope(Dispatchers.IO)
//    scope.launch {
//        runBlocking<Unit> {
//            printlnThread("main")
//            launch(Dispatchers.IO) {
//                printlnThread("launch1")
//                launch {
//                    launch {
//                        launch {
//                            printlnThread("launch 2")
//                        }
//                    }
//                }
//            }
//        }
//    }.join()
//}
