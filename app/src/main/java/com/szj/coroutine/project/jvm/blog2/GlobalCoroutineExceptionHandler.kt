package com.szj.coroutine.project.jvm.blog2

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlin.coroutines.CoroutineContext

/*
 * 作者: 史大拿
 * 时间: 2023/2/13$ 19:24$
 */
class GlobalCoroutineExceptionHandler : CoroutineExceptionHandler {
    override val key: CoroutineContext.Key<*>
        get() = CoroutineExceptionHandler

    override fun handleException(context: CoroutineContext, exception: Throwable) {
            println("全局捕获异常:$exception context:\t${context}")
    }
}