package com.szj.coroutine.project.jvm.blog4

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
 * 作者: 史大拿
 * 时间: 2023/2/18$ 16:46$
 */

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

fun main() = runBlocking<Unit> {
    val sharedFlow = MutableSharedFlow<Int>(1, 256)

    // 发送方
    launch {
        repeat(10) {
            sharedFlow.emit(it)
            println("Sent: $it")
            delay(100)
        }
    }

    // 接收方
    launch {
//        delay(500) // 每0.5秒接受一个值
        sharedFlow.collect {
            delay(3000) // 每0.3秒消费一个值
            println("Received: $it")
        }
    }
}
