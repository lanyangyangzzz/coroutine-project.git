package com.szj.coroutine.project.jvm.blog5

import com.szj.coroutine.project.jvm.util.printlnThread
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.concurrent.thread
import kotlin.coroutines.suspendCoroutine

/*
 * 作者: 史大拿
 * 时间: 2023/2/21$ 15:42$
 */

private fun requestLoginNetworkData(
    account: String, pwd: String, result: Result<String>.() -> Unit
) {
    if (account == "123456789" && pwd == "666666") {
        result.invoke(Result.success("123"))
    } else {
        result.invoke(Result.failure(RuntimeException("登陆失败")))
    }
}

fun main() {
    thread { // 开启一个线程
        requestLoginNetworkData("987654321", "123456") {

            onSuccess {
                printlnThread("登陆成功 :$it")
            }.onFailure {
                printlnThread("登陆失败 :${it.message}")
            }
        }

    }
}