package com.szj.coroutine.project.jvm.util

import com.szj.coroutine.project.bean.NetWorkBean
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

/*
 * 作者: 史大拿
 * 时间: 2023/2/22$ 10:28$
 */
interface NetWorkApi {
    companion object {
        const val BASE_URL = "https://api.apiopen.top"
    }

    @GET("/api/getHaoKanVideo?page=0&size=2")
     fun requestNetWorkData(): Call<NetWorkBean>
}